import requests
from bs4 import BeautifulSoup
import csv
contPage = 0
cont = 0

#abrindo arquivo que utilizarei para guardar todas as páginas
with open('dados_composicao_alimentos.csv', mode='w', encoding='utf-8', newline='') as csv_file1:

    #esse primeiro request eu fiz para guardar o cabeçalho da tabela na list_head
    page = requests.get('http://www.tbca.net.br/base-dados/composicao_alimentos.php?pagina=1&atuald=1')
    soup = BeautifulSoup(page.text, 'html.parser')
    landing_page = soup.find(class_="landing-page sidebar-collapse")
    content_page = landing_page.find(class_='wrapper')
    filtred_content = content_page.find(class_="col-12 col-md-12 col-xl-12 py-md-3 pl-md-5 bd-content")
    table = filtred_content.find(class_="bd-example")
    table_head = table.find('thead')
    
    list_head = []
    list_body = []
                              
    #percorrendo o cabeçalho da tabela e utilizando o método .append para guardar as colunas na lista           
    for colum in table_head.find_all('th'):
        list_head.append(colum.text)

    #criando um DictWriter para salvar o cabeçalho através do método .writeheader
    writer = csv.DictWriter(csv_file1, fieldnames=list_head)
    writer.writeheader()

    #loop para percorrer toda as páginas e refazer o request, pois as atualizações mudam a cada request
    #pode ser otimizado
    for x in range(1,6):
        for y in range(1,11):
            cont=0
            contPage+=1
            #condição de parada do bot + debug
            #pode ser otimizada
            if (contPage>43):
                print("Bot chegou na última página")
                break
            else:
                #concatenando para criar um request em cada page 
                #recriando a NavigableTree para inserir os dados no .csv que está aberto
                allPage = requests.get('http://www.tbca.net.br/base-dados/composicao_alimentos.php?pagina={}&atuald={}'.format(contPage,x))
                print(allPage.status_code + contPage)
                allSoup = BeautifulSoup(allPage.text, 'html.parser')
                landing_page = allSoup.find(class_="landing-page sidebar-collapse")
                content_page = landing_page.find(class_='wrapper')
                filtred_content = content_page.find(class_="col-12 col-md-12 col-xl-12 py-md-3 pl-md-5 bd-content")
                table = filtred_content.find(class_="bd-example")
                table_body = table.find('tbody')
                writerDict = {}
                #Bryan loop que utiliza o método .contents para navegar como um vetor através de cada linha
                #utilização do método .update do DictWriter instanciado mais acima na linha 27
                #guardando os dados da tabela utilizando o método .writerow + debug
                while (cont <= (len(table_body) - 1)):
                    valueList = table_body.contents[cont].find_all('td')
                    for x in range(len(list_head)):
                        writerDict.update({f'{list_head[x]}':f'{valueList[x].text}'})            

                    writer.writerow(writerDict)    
                    print(writerDict)
                    cont += 1
                            

                
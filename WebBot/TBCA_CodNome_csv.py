#este bot trata-se de uma coleta de informações do site TBCA, registrando apenas Código e Nome em um .csv

#import de bibliotecas utilizadas para o bot

import requests
from bs4 import BeautifulSoup
import csv
cont = 0

#abrindo o arquivo em modo escrita "w" de write e definindo os nome do cabeçalho do .csv
with open('dados_nomeCod_TBCA.csv', mode='w', encoding='utf-8', newline='') as csv_file:
    fieldnames = ['Cod', 'Nome']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader() #salva o cabeçalho com os valores da lista fieldnames

    #for que percorre todo o site e concatena o número das páginas para fazer o request de todas elas
    for x in range(1,5):
        for y in range(1,11):
            #cont para concatenar o numero de paginas, recebe 1 toda vez que o laço roda
            cont+=1
            page = requests.get('http://www.tbca.net.br/base-dados/composicao_estatistica.php?pagina={}&atuald={}'.format(cont,x))
            #debug
            print('http://www.tbca.net.br/base-dados/composicao_estatistica.php?pagina={}&atuald={}'.format(cont,x))
            
            #38 páginas, condição de parada do looping e msg de fim de bot
            if(cont>=54):
                print('Fim do bot!!')
                break
            #navegando pela árvored e TAGS que o bs4 cria como um objeto
            #uso do método find() da bs4 na maioria das buscas, é só olhar o site e inspecionar para entender
            soup = BeautifulSoup(page.text, 'html.parser')
            landing_page = soup.find(class_="landing-page sidebar-collapse")
            content_page = landing_page.find(class_='wrapper')
            filtred_contet = content_page.find(class_="col-12 col-md-12 col-xl-12 py-md-3 pl-md-5 bd-content")
            table = filtred_contet.find(class_="bd-example")
            table_head = table.find(class_="thead-light")
            table_body = table.find('tbody')
            #lista desnecessário neste código pois estou salvando tudo em um .csv, porém bom para aprender
            lista_CodNome = []
        
            
            #acessando todas as tr (linhas) através do looping e invocando o método find_all() da bs4
            for linha in table_body.find_all('tr'):
                cod_ali = str(linha.td.text)
                nome_ali = str(linha.td.find_next_sibling().text)
                #find_next_sibling() procura o próximo primo da tag que acessamos
                    
                writer.writerow({'Cod': cod_ali, 'Nome': nome_ali})
                #whiterow salva de acordo com o cabeçalho o valor da variável

                #concatenando e salvando na lista CodNome usando o método append()
                cod_nome = cod_ali + '; ' + nome_ali
                lista_CodNome.append(cod_nome)

      

 
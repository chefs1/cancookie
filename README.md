<div align="center">
   
   ##  🚧 Projeto Cancook em andamento 🚧

   <h1>
      <a href="">
         <img src="/cancookweb/cancook/src/Assets/Img/IconePrincipalComletra.png" height="300px"/>
      </a>

      Cancook é um web site que fornece informação nutricionais, podendo calcular todos nutrientes dos alimentos...  
   </h1>
</div>

# 🚀 Indice

- [Sobre](#-mais-sobre)
- [Funcionalidades](#-funcionalidades)
- [Tecnologias Utilizadas](#-tecnologias)
- [Links Disponiveis](#-links)

## ⌨️ Desenvolvedor

- [Allan Takeuchi Bustamante](https://gitlab.com/allantak)
- [Lucas Viilela de Oliveira](https://gitlab.com/lucas.vilela.oliveira)
- [Wendel Demetrio Bispo](https://gitlab.com/hey.wendel95)


## 🔖&nbsp; Mais sobre

O **Cancook** tem como propósito trazer os dados nutricionais de uma refeição, onde o usuário poderá através de um site  buscar por alimentos seja pelo nome ou grupo a qual este pertence. Os dados são retirados do site TBCA - Tabela Brasileira de Composição Alimentar e armazenados em um banco de dados local.


## ✨ Funcionalidades

- Listagem dos alimentos;
- Calculo de carboidratos, proteinas, gorduras e kcal;

## 🔨 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

- [Node.js](https://nodejs.org/en/)
- [React.js](https://pt-br.reactjs.org/)
- [Express](https://expressjs.com/pt-br/)


## ⚠ Links

#### Mockup - UI Design:

 - [Link do design do app](https://www.figma.com/community/file/981564139352986993/CanCookie)








import React, {useState, useEffect}   from "react";
import Search from '../../Components/Search/index';
import Abas from '../../Components/Abas/index';
import Header from '../../Components/Header/index';
import Select from "../../Components/SelectGrupo/index";
import Footer from '../../Components/Footer/index';
import logoLado from '../../Assets/Img/IconePrincipalLetraLado.png';

import {Link} from 'react-router-dom';
import Axios from 'axios';

// 
import { Icon, InlineIcon } from '@iconify/react';
import calculator24Filled from '@iconify-icons/fluent/calculator-24-filled';





import "./styles.css";


const Pesquisa = ({match}) => {
    const [alimentoData, setAlimentoData] = useState([]);
    const [refeicao, setRefeicao] = useState([]);
    const url = match.params.id ;
    const [listRefeicao, setListRefeicao] = useState([]);
    

    useEffect(() => {
        async function alimento() {
            Axios.get("http://localhost:3001/alimento").then((response) => {
                setAlimentoData(response.data);
            });
        };
        
        async function refeicao(){
            Axios.get(`http://localhost:3001/Pesquisa/${url}`).then((response) =>{
                setRefeicao(response.data);

    
            });    
        }

        
        
        alimento();
        refeicao();
    }, []);

    
    
    async function ListRefeicao(id){
        Axios.get(`http://localhost:3001/Pesquisa/${id}`).then((response) =>{
            let arr = listRefeicao.concat(response.data);
            setListRefeicao(arr);
            console.log("List confirmado", arr);
            

        });    
    }

    const ListaRefeicao  = listRefeicao.concat(refeicao);

    const total = ListaRefeicao.reduce(
        (prevValue, currentValue) => prevValue + currentValue.carboidratoTotalg,
        0
      );
   
    

    
    return (
        
        <div className="Container">
            <Header/>
            

            <div className="Conteinar-list">

                <div className="AreaImg">
                    <img alt="Logo com Letra" src={logoLado}/>
                </div>

                <div className="AreaPost">
                    <Select className="AreaSelect"/>
                    <Search className="AreaPes"/>
                </div>


                <div className="Area">

                    <div className="AreaList">
                        <div className="List">
                            {alimentoData.map((data)=>{
                                return (
                                    <Link  key={data.idalimento} onClick={()=>ListRefeicao(data.idalimento)} to={`/Pesquisa/${data.idalimento}`}><Abas data={data}/></Link>
                                );
                            })}
                        </div>
                    </div>

                    <div className="AreaRefeicao">

                        <div  className="Cabecalho">
                            <h3 className="TextCabe">Calcule sua refeição</h3>
                            <Icon icon={calculator24Filled} color="#ffff" width="30px" height="30px"/>
                        </div>

                        <div className="List">

                        {refeicao.map((data)=>{
                                return (
                                    <Abas key={data.idalimento} data={data}/>
                                );
                            })}

                        {listRefeicao.map((data)=>{
                                return (
                                    <Abas key={data.idalimento} data={data}/>

                                    
                                );
                            })}
                            
                        </div>

                        
                    </div>

                </div>

                <div className="AreaFooter">
                            <h3 className="TextCabe">Resultado</h3>
                            <div className="Result">
                                <p className="TextCabe" >{total} KCAL</p>
                            </div>
                        </div>


            </div>

            <div className="Content-Footer">
                <Footer />
            </div>
            
        </div>
    );
}

export default Pesquisa;
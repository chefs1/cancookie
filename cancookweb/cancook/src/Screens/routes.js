import React from 'react';

import { BrowserRouter, Route, Switch, Redirect} from "react-router-dom";

import Home from './Home/index';
import Pesquisa from './Pesquisa/index';

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/Pesquisa/:id" component={Pesquisa}/>
        </Switch>
    </BrowserRouter>
);

export default Routes;
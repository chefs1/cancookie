import React, {useState, useEffect} from "react";
import Search from '../../Components/Search/index';
import Abas from '../../Components/Abas/index';
import Header from '../../Components/Header/index';
import Select from '../../Components/SelectGrupo/index';
import logo from '../../Assets/Img/IconePrincipalComletra.png';
import Footer from '../../Components/Footer/index';
import "../Home/styles.css";

import Axios from 'axios';
import {Link} from 'react-router-dom';


const Home = () => {
    const [alimentoData, setAlimentoData] = useState([]);

    useEffect(() => {
        async function alimento() {
            Axios.get("http://localhost:3001/alimento").then((response) => {
                setAlimentoData(response.data);
                console.log(response.data);
            });
        };
        alimento();
    }, []);



    return (
        
        <div className="Content">
            <Header/>

            <div className="Content-list">
                <div className="ImgArea">
                    <img alt="Logo com Letra" src={logo}/>
                </div>

                <div className="PostArea">

                    <Select/>
                    <Search/>

                </div>

                <div className="ListArea">

                    <div className="List">

                        {alimentoData.map((data)=>{
                            return (
                                
                                <Link key={data.idalimento} to={`/Pesquisa/${data.idalimento}`}><Abas key={data.idalimento} data={data}/></Link>
                                
                            );
                        })}

                    </div>

                </div>
            </div>
            <div className="Content-Footer">
                <Footer />
            </div>

        </div>
    );
}

export default Home;
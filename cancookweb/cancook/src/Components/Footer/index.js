import React from 'react';
import './styles.css';
import logo from '../../Assets/Img/IconePrincipalLetraLado.png';

export default class Header extends React.Component {
  render() {
    return (
      <div className="Footer">
        <img alt="Logo Navbar" src={logo} />
        <div className="Git">
            <a href="https://gitlab.com/chefs1/cancookie" target="_blank" style={{ textDecoration: 'none', color: '#1B2C1A' }}>gitlab.com/chefs1/cancookie</a>
        </div>
        <div className="Linkedin">
            <a href="https://www.linkedin.com/in/allan-takeuchi-8239441b1/" target="_blank" style={{ textDecoration: 'none', color: '#1B2C1A' }}> Linkedin Allan Tak</a><br></br>
            <a href="https://www.linkedin.com/in/lucas-vilela-de-oliveira-7b90131aa/" target="_blank" style={{ textDecoration: 'none', color: '#1B2C1A' }}> Linkedin Lucas Vilela</a><br></br>
            <a href="" target="_blank" style={{ textDecoration: 'none', color: '#1B2C1A' }}> Linkedin João Felipe</a><br></br>
            <a href="" target="_blank" style={{ textDecoration: 'none', color: '#1B2C1A' }}> Linkedin Wendel</a><br></br>
        </div>
      </div>
    )
  }
}
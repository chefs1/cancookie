import React  from "react";
import "./styles.css";

// npm install --save-dev @iconify/react @iconify-icons/octicon
import { Icon } from '@iconify/react';
import searchIcon from '@iconify-icons/octicon/search';


const Search = () => {
    return (
                <div className="areaInput">
                    <input className="inputPrincipal" type="text" placeholder="Pesquise seu alimento"/>
                    <button className="ButtonPesqui"><Icon icon={searchIcon} /></button>
                </div>
    )
}

export default Search;

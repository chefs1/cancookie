import React from 'react';
import './styles.css';
import logo from '../../Assets/Img/IconePrincipalLetraLado.png';

export default class Header extends React.Component {
  render() {
    return (
      <div className="Header">
          <a href="/" title="Cancook">Cancook
          <img alt="Logo Navbar" src={logo} />
          </a>
      </div>
    )
  }
}
import React from 'react';
import "./styles.css";

export default class Abas extends React.Component {
  
    render() {
        const className ='btn';
        return(
            
                <button 
                    className={className}
                    onMouseDown={this.toggleTouched}
                    onMouseUp={this.handleMouseUp}
                    >
                        <div className="AreaText">
                            <h3>{this.props.data.nome}</h3>
                            <p className="TextDisc">{this.props.data.ingrediente}</p>
                        </div>

                        <div className="AreaMacros">

                            <div className="BlocoCarb">
                                <h3 className="TitleBloco">Carb</h3>
                                <p className="DiscCarb">{this.props.data.carboidratoTotalg}g</p>
                            </div>
                            <div className="Bloco">
                                <h3 className="TitleBloco">Proteina</h3>
                                <p className="DiscProt">{this.props.data.proteinag}g</p>
                            </div>
                            <div className="Bloco">
                                <h3 className="TitleBloco" >Gordura</h3>
                                <p className="DiscGord">{this.props.data.lipidiosg}g</p>
                            </div>
                        </div>

                        
                        
                </button>

            
            
            
            
        );

    }

}
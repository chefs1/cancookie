import React, {useState} from 'react';
import './styles.css'
export default function SelectGrupo () {
  
  const [selectValue, setSelectValue] = useState('Grupo Alimentar')

  const handleSubmit = (e) => {
    e.preventDefault();
    alert(`Submitting Name ${selectValue}`)
  }

  return (
    <form onSubmit={handleSubmit } className="formSelect" >
      <label className="labelSelect" >
        <select className = "boxSelect" value={(selectValue)} onChange={evt => setSelectValue(evt.target.value)}>
          <option value="Todos">Selecione</option>
          <option value="Alimentos Industrializados">Alimentos Industrializados</option>
          <option value="Alimentos para Fins Especiais" >Alimentos para Fins Especiais</option>
          <option value="Bebidas">Bebidas</option>
          <option value="Carnes e Derivados">Carnes e Derivados</option>
          <option value="Cereais e Derivados">Cereais e Derivados</option>
          <option value="Fast Food ">Fast Food</option>
          <option value="Frutas e Derivados">Frutas e Derivados</option>
          <option value="Gorduras e Azeites">Gorduras e Azeites</option>
          <option value="Leguminosas e Derivados">Leguminosas e Derivados</option>
          <option value="Leite e Derivados">Leite e Derivados</option>
          <option value="Miscelânea">Miscelânea</option>
          <option value="Ovos e Derivados">Ovos e Derivados</option>
          <option value="Pescados e Frutos do Mar">Pescados e Frutos do Mar</option>
          <option value="Produtos Açucarados">Produtos Açucarados</option>
          <option value="Sementes e Oleaginosas">Sementes e Oleaginosas</option>
          <option value="Vegetais e Derivados">Vegetais e Derivados</option>
          <option>Oi sou o que você clicou {selectValue}</option>

        </select>
      </label>
    </form>
  );

 }

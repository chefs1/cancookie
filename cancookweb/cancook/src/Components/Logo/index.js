import React from 'react';
import './styles.css';
import logo from '../../Assets/Img/IconePrincipalComletra.png';

export default class Logo extends React.Component {
    render() {
      return(
        <div className="Logo">
          <img alt="Logo com Letra" src={logo} />
        </div>
      )
    }
  }